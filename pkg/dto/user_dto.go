package dto

import validation "github.com/go-ozzo/ozzo-validation"

type UserDTO struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	Fullname string `json:"fullname"`
	Address  string `json:"address"`
}

func (u UserDTO) Validation() error {
	err := validation.ValidateStruct(&u,
		validation.Field(&u.Email, validation.Required),
		validation.Field(&u.Password, validation.Required),
		validation.Field(&u.Fullname, validation.Required),
		validation.Field(&u.Address, validation.Required),
	)

	if err != nil {
		return err
	}
	return nil
}
