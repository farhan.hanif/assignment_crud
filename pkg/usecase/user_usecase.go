package usecase

import (
	"crud_echo/pkg/domain"
	"crud_echo/pkg/dto"
	"crud_echo/shared/password"
	"github.com/golang-jwt/jwt"
	"github.com/mitchellh/mapstructure"
	"time"
)

type UserUsecase struct {
	UserRepository domain.UserRepository
}

func NewUserUsecase(userRepository domain.UserRepository) domain.UserUsecase {
	return &UserUsecase{
		UserRepository: userRepository,
	}
}

func (uu UserUsecase) GetUsers() ([]domain.User, error) {
	return uu.UserRepository.GetUsers()
}

func (uu UserUsecase) GetUser(id int) (domain.User, error) {
	return uu.UserRepository.GetUser(id)
}

func (uu UserUsecase) GetUserByEmail(email string) (domain.User, error) {
	return uu.UserRepository.GetUserByEmail(email)
}

func (uu UserUsecase) CreateUser(req dto.UserDTO) error {
	var user domain.User
	mapstructure.Decode(req, &user)
	hash, _ := password.HashPassword(user.Password)
	user.Password = hash
	return uu.UserRepository.CreateUser(user)
}

func (uu UserUsecase) UpdateUser(id int, req dto.UserDTO) error {
	var user domain.User
	mapstructure.Decode(req, &user)
	hash, _ := password.HashPassword(user.Password)
	user.Password = hash
	return uu.UserRepository.UpdateUser(id, user)
}

func (uu UserUsecase) DeleteUser(id int) error {
	return uu.UserRepository.DeleteUser(id)
}

func (uu UserUsecase) Login(email string, pass string) (string, error) {
	res, err := uu.UserRepository.GetUserByEmail(email)
	if err != nil {
		return "", err
	}
	_, err2 := password.ComparePassword(pass, res.Password)
	if err2 != nil {
		return "", err2
	}

	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["expired"] = time.Now().Add(time.Hour * 72).Unix()

	t, err3 := token.SignedString([]byte("secret"))
	if err3 != nil {
		return "", err3
	}
	return t, nil
}
