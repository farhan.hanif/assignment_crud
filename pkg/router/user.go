package router

import (
	"crud_echo/pkg/controller"
	"crud_echo/pkg/repository"
	"crud_echo/pkg/usecase"
	"database/sql"
	"github.com/labstack/echo/v4"
)

func NewUserRouter(e *echo.Echo, g *echo.Group, db *sql.DB) {
	ur := repository.NewUserRepository(db)
	uu := usecase.NewUserUsecase(ur)
	uc := &controller.UserController{
		UserUsecase: uu,
	}

	g.GET("/", uc.GetUsers)
	g.GET("/:id", uc.GetUser)
	g.POST("/", uc.CreateUser)
	g.PUT("/:id", uc.UpdateUser)
	g.DELETE("/:id", uc.DeleteUser)
}
