package repository

import (
	"crud_echo/pkg/domain"
	"database/sql"
)

type UserRepository struct {
	db *sql.DB
}

func NewUserRepository(db *sql.DB) domain.UserRepository {
	return &UserRepository{
		db: db,
	}
}

func (ur UserRepository) GetUsers() ([]domain.User, error) {
	query := `SELECT * FROM users`
	rows, err := ur.db.Query(query)
	var users []domain.User
	for rows.Next() {
		var res domain.User
		err2 := rows.Scan(&res.ID, &res.Email, &res.Password, &res.Fullname, &res.Address)
		if err2 != nil {
			return nil, err2
		}
		users = append(users, res)
	}
	return users, err
}

func (ur UserRepository) GetUser(id int) (domain.User, error) {
	var res domain.User
	query := `SELECT * FROM users WHERE id = $1`
	err := ur.db.QueryRow(query, id).Scan(&res.ID, &res.Email, &res.Password, &res.Fullname, &res.Address)
	return res, err
}

func (ur UserRepository) CreateUser(user domain.User) error {
	query := "INSERT INTO users (Email, Password, Fullname, Address) VALUES ($1, $2, $3, $4)"
	stmt, err := ur.db.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err2 := ur.db.Exec(query, user.Email, user.Password, user.Fullname, user.Address)

	if err2 != nil {
		return err2
	}
	return nil
}

func (ur UserRepository) GetUserByEmail(email string) (domain.User, error) {
	var res domain.User
	query := "SELECT * FROM users WHERE email = $1"
	err := ur.db.QueryRow(query, email).Scan(&res.ID, &res.Email, &res.Password, &res.Fullname, &res.Address)
	return res, err
}

func (ur UserRepository) UpdateUser(id int, req domain.User) error {
	query := `UPDATE users SET email = $1, password = $2, fullname = $3, address = $4 WHERE id = $5`
	stmt, err := ur.db.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(req.Email, req.Password, req.Fullname, req.Address, id)
	if err2 != nil {
		return err2
	}
	return nil
}

func (ur UserRepository) DeleteUser(id int) error {
	query := `DELETE FROM users WHERE id = $1`
	_, err := ur.db.Exec(query, id)
	return err
}
