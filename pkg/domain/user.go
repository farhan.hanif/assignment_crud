package domain

import "crud_echo/pkg/dto"

type User struct {
	ID       int    `json:"id"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Fullname string `json:"fullname"`
	Address  string `json:"address"`
}

type UserRepository interface {
	GetUsers() ([]User, error)
	GetUser(id int) (User, error)
	GetUserByEmail(email string) (User, error)
	CreateUser(req User) error
	UpdateUser(id int, req User) error
	DeleteUser(id int) error
}

type UserUsecase interface {
	GetUsers() ([]User, error)
	GetUser(id int) (User, error)
	GetUserByEmail(email string) (User, error)
	CreateUser(req dto.UserDTO) error
	UpdateUser(id int, req dto.UserDTO) error
	DeleteUser(id int) error
	Login(email string, pass string) (string, error)
}
