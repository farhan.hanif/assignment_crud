package controller

import (
	"crud_echo/pkg/domain"
	"crud_echo/pkg/dto"
	"crud_echo/shared/response"
	"fmt"
	"github.com/labstack/echo/v4"
	"net/http"
	"strconv"
)

type UserController struct {
	UserUsecase domain.UserUsecase
}

func (uc *UserController) GetUsers(c echo.Context) error {
	resp, err := uc.UserUsecase.GetUsers()
	if err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", resp)
}

func (uc *UserController) GetUser(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	resp, err := uc.UserUsecase.GetUser(id)
	fmt.Println(err)
	if err != nil {
		return response.SetResponse(c, http.StatusNotFound, "user not found", nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", resp)
}

func (uc *UserController) CreateUser(c echo.Context) error {
	var userdto dto.UserDTO
	if err := c.Bind(&userdto); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, "bad request", nil)
	}
	if err := userdto.Validation(); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}
	if _, err := uc.UserUsecase.GetUserByEmail(userdto.Email); err == nil {
		return response.SetResponse(c, http.StatusInternalServerError, "email already exist", nil)
	}
	if err := uc.UserUsecase.CreateUser(userdto); err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", nil)
}

func (uc *UserController) UpdateUser(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	var userdto dto.UserDTO
	if err := c.Bind(&userdto); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, "bad request", nil)
	}
	if _, err := uc.UserUsecase.GetUser(id); err != nil {
		return response.SetResponse(c, http.StatusNotFound, err.Error(), nil)
	}
	if err := userdto.Validation(); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}
	if err := uc.UserUsecase.UpdateUser(id, userdto); err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", nil)
}

func (uc *UserController) DeleteUser(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))

	if _, err := uc.UserUsecase.GetUser(id); err != nil {
		return response.SetResponse(c, http.StatusNotFound, err.Error(), nil)
	}

	if err := uc.UserUsecase.DeleteUser(id); err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", nil)
}
